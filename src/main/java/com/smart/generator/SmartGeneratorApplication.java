package com.smart.generator;

import com.smart.generator.config.ContextConfig;
import com.smart.generator.config.GeneratorConfig;

/**
 * @author Administrator
 */
public class SmartGeneratorApplication {

    public static void main(String[] args) {
        GeneratorConfig generatorConfig = new GeneratorConfig();
        //全局变量配置
        ContextConfig config=new ContextConfig();
        //模块名称
        config.setModelName("channel");
        //作者
        config.setAuthor("baohao-jia");
        //前缀取消-用于进行生成是的表前缀取消
        config.setTablePrefix(new String[]{"channel_"});
        //前缀过滤-可根据指定前缀生成指定表的代码
        config.setTablePrefixStr("channel_");
        //根目录
        config.setParent("com.smart");
        //数据库名称
        config.setDataSourceBank("smart-boot-credit-prepose_v0.1.0");
        //继承类绝对路径
        config.setSuperControllerClass("com.smart.common.inheritance.controller.BaseController");
        //继承类绝对路径
        //config.setSuperEntityClass("com.smart.common.inheritance.BaseEntity");
        config.setSuperEntityClass(null);
        //标记是否生成当前库的所有表
        config.setAll(true);
        generatorConfig.doMpGeneration(config);
    }

}

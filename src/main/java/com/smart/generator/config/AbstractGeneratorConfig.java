package com.smart.generator.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.querys.DB2Query;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ProjectName: smart-generator
 * @Package: com.smart.generator.config
 * @ClassName: AbstractGeneratorConfig
 * @Author: Administrator
 * @Description: 代码生成器抽象配置
 * @Date: 2021-09-01 18:31
 * @Version: 1.0
 */
public abstract class AbstractGeneratorConfig {

    /**
     * mybatis-plus代码生成器配置
     */

    GlobalConfig globalConfig = new GlobalConfig();

    DataSourceConfig dataSourceConfig = new DataSourceConfig();

    StrategyConfig strategyConfig = new StrategyConfig();

    PackageConfig packageConfig = new PackageConfig();

    TemplateConfig templateConfig=new TemplateConfig();


    protected abstract void config();

    public void init() {
        config();
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别，默认vm，xml不输出
        templateConfig.setEntity("templates/java/entity.java");
        templateConfig.setService("templates/java/service.java");
        templateConfig.setServiceImpl("templates/java/serviceImpl.java");
        templateConfig.setController("templates/java/controller.java");
        templateConfig.setMapper("templates/java/mapper.java");
        templateConfig.setXml("templates/java/mapper.xml");
    }

    public void doMpGeneration(ContextConfig config) {
        init();
        AutoGenerator autoGenerator = new AutoGenerator();
        //加载全局配置
        globalConfig.setOutputDir(config.getOutputDir()+"//main//java");
        globalConfig.setAuthor(config.getAuthor());
        //加载数据库设置
        dataSourceConfig.setUsername(config.getDataSourceUser());
        dataSourceConfig.setPassword(config.getDataSourcePwd());
        dataSourceConfig.setUrl("jdbc:mysql://"+config.getDataSourceUrl()+"/"+config.getDataSourceBank()+"?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8");
        if(!config.getAll()) {
            //要生成的数据库表，不设置则为数据库全表
            strategyConfig.setInclude(GeneratorConfig.scanner("表名，多个英文逗号分割").split(","));
        }
        //过滤前缀不为空则启动sql模糊过滤功能-指定生成表此项配置不生效
        if(StringUtils.isNotEmpty(config.getTablePrefixStr()) && config.getAll()){
            strategyConfig.setEnableSqlFilter(true);
            strategyConfig.setLikeTable(new LikeTable(config.getTablePrefixStr()));
        }
        //实体类集成自定义封装的父类
        strategyConfig.setSuperEntityClass(config.getSuperEntityClass());
        //继承自定义封装的controller父类
        strategyConfig.setSuperControllerClass(config.getSuperControllerClass());
        //设置根目录
        packageConfig.setParent(config.getParent());
        // 此处可以修改为您的表前缀,生成会取消前缀
        strategyConfig.setTablePrefix(config.getTablePrefix());

        autoGenerator.setGlobalConfig(globalConfig);
        //加载数据源配置
        autoGenerator.setDataSource(dataSourceConfig);
        //加载模板
        autoGenerator.setTemplate(templateConfig);
        //加载策略配置
        autoGenerator.setStrategy(strategyConfig);
        //加载包配置
        autoGenerator.setPackageInfo(packageConfig);

        //自定义配置
        // 注入自定义配置，可以在 VM 中使用 cfg.*** 【可无】  ${cfg.***}
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("modelName", config.getModelName());
                this.setMap(map);
            }

        };

        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义 api.js 生成
        focList.add(new FileOutConfig("/templates/js/api.js.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return config.getOutputDir()+"//vue//js//" + StrUtil.lowerFirst(tableInfo.getEntityName()) + ".js";
            }
        });
        injectionConfig.setFileOutConfigList(focList);
        //自定义 index.vue 生成
        focList.add(new FileOutConfig("/templates/vue/index.vue.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return config.getOutputDir()+"//vue//views//"+StrUtil.lowerFirst(tableInfo.getEntityName())+"//index.vue";
            }
        });
        injectionConfig.setFileOutConfigList(focList);

        autoGenerator.setCfg(injectionConfig);
        autoGenerator.execute();
    }
}

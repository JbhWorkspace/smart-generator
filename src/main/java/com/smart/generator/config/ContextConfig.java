package com.smart.generator.config;

/**
 * @ProjectName: smart-generator
 * @Package: com.smart.generator.config
 * @ClassName: ContextConfig
 * @Author: Administrator
 * @Description: 自定义全局配置
 * @Date: 2021-09-03 16:23
 * @Version: 1.0
 */
public class ContextConfig {
    /**
     * 项目存储根目录
     */
    private String outputDir = "D://generator";

    /**
     * 业务模块
     */
    private String modelName="system";

    /**
     * 作者名称
     */
    private String author="smart";

    /**
     * 表前缀
     */
    private String[] tablePrefix=null;

    /**
     * 表前缀-单个主要用于根据数据表前缀生成特定前缀
     */
    private String tablePrefixStr=null;

    /**
     * 根目录
     */
    private String parent=null;

    /**
     * 数据库相关
     */
    private String dataSourceUser="user"; //数据库账号

    private String dataSourcePwd="pwd"; //数据库密码

    private String dataSourceUrl="ip:port"; //数据库地址与端口

    private String dataSourceBank=""; //数据库名称

    /**
     * 是否生成数据库全表
     */
    private Boolean isAll=false;

    private String superEntityClass="com.smart.common.inheritance.BaseEntity";

    private String superControllerClass="com.smart.common.core.web.BaseController";



    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String[] getTablePrefix() {
        return tablePrefix;
    }

    public void setTablePrefix(String[] tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getDataSourceUser() {
        return dataSourceUser;
    }

    public void setDataSourceUser(String dataSourceUser) {
        this.dataSourceUser = dataSourceUser;
    }

    public String getDataSourcePwd() {
        return dataSourcePwd;
    }

    public void setDataSourcePwd(String dataSourcePwd) {
        this.dataSourcePwd = dataSourcePwd;
    }

    public String getDataSourceUrl() {
        return dataSourceUrl;
    }

    public void setDataSourceUrl(String dataSourceUrl) {
        this.dataSourceUrl = dataSourceUrl;
    }

    public String getDataSourceBank() {
        return dataSourceBank;
    }

    public void setDataSourceBank(String dataSourceBank) {
        this.dataSourceBank = dataSourceBank;
    }

    public Boolean getAll() {
        return isAll;
    }

    public void setAll(Boolean all) {
        isAll = all;
    }

    public String getSuperEntityClass() {
        return superEntityClass;
    }

    public void setSuperEntityClass(String superEntityClass) {
        this.superEntityClass = superEntityClass;
    }

    public String getSuperControllerClass() {
        return superControllerClass;
    }

    public void setSuperControllerClass(String superControllerClass) {
        this.superControllerClass = superControllerClass;
    }

    public String getTablePrefixStr() {
        return tablePrefixStr;
    }

    public void setTablePrefixStr(String tablePrefixStr) {
        this.tablePrefixStr = tablePrefixStr;
    }
}

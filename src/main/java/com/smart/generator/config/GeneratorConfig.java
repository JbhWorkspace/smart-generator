package com.smart.generator.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

/**
 * @ProjectName: smart-generator
 * @Package: com.smart.generator.config
 * @ClassName: GeneratorConfig
 * @Author: Administrator
 * @Description: 代码生成器默认配置
 * @Date: 2021-09-02 9:20
 * @Version: 1.0
 */
public class GeneratorConfig extends AbstractGeneratorConfig {


    protected void globalConfig() {
        //同名文件是否覆盖
        globalConfig.setFileOverride(true);
        globalConfig.setOpen(false);
        // 不需要ActiveRecord特性的请改为false
        globalConfig.setActiveRecord(false);
        // XML 二级缓存
        globalConfig.setEnableCache(false);
        // XML ResultMap
        globalConfig.setBaseResultMap(true);
        // XML columList
        globalConfig.setBaseColumnList(false);

        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        //controller层命名
        globalConfig.setControllerName("%sController");
        //service层命名
        globalConfig.setServiceName("I%sService");
        //service实现类命名
        globalConfig.setServiceImplName("%sServiceImpl");
        //mapper文件命名
        globalConfig.setMapperName("%sMapper");
        //xml的mapper文件命名
        globalConfig.setXmlName("%sMapper");
        globalConfig.setIdType(IdType.ASSIGN_ID);
        //数据表的注解放到实体类中
        globalConfig.setSwagger2(false);
    }

    protected void dataSourceConfig() {
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
    }

    protected void strategyConfig() {
        //controller是否restful风格
        strategyConfig.setRestControllerStyle(true);
        //类名生成策略：驼峰命名
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setCapitalMode(true);
        //lombok注解支持
        strategyConfig.setEntityLombokModel(true);
        //mybatis-plus 注解
        strategyConfig.setEntityTableFieldAnnotationEnable(true);
        //配置父类实体类字段子类中不生成 ** 此项需要配置为数据库命名字段
        strategyConfig.setSuperEntityColumns("create_id","create_time","update_id","update_time","del_flag","status","version","remark");
    }


    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    @Override
    protected void config() {
        globalConfig();
        dataSourceConfig();
        strategyConfig();
        //packageConfig(config.getParent());
    }
}
